import React from "react";
import "./App.css";
import { Container } from "reactstrap";
import Services from "./components/Services";
import Providers from "./components/Providers";

function App() {
  return (
    <div className="App">
      <Container>
        <center>
          <h2>Services</h2>
        </center>
        <Services />
        <center>
          <h2>Providers</h2>
        </center>
        <Providers />
      </Container>
    </div>
  );
}

export default App;
