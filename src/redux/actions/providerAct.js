import { GET_PROVIDER_LOADING, GET_PROVIDER_SUCCESS, GET_PROVIDER_ERROR, GET_PROVIDER_SUCCESS_FILTER } from "../actionTypes"
import { getProvidersApi } from '../api';

export const providerAct = filter => {
    return async (dispatch, getState) => {
        dispatch({ type: GET_PROVIDER_LOADING })
        try {
            let res;
            if (!!getState().provider.providerData && getState().provider.providerData.data.length > 0){
                res = getState().provider.providerData;
            }else{
                res = await getProvidersApi();
            }
            if(!!filter){
                if (!!res.included && res.included.length > 0){
                    const param = res.included.filter(item => {
                        return item.attributes.service === filter
                    });
                    if(!!param && !!res.data && res.data.length > 0 && param.length > 0){
                        const finalResult = param.map(attr => {
                            const result = res.data.find(pro => {
                                return attr.id === pro.relationships[attr.type].data[0].id
                            })
                            return result;
                        })
                        return dispatch({ type: GET_PROVIDER_SUCCESS_FILTER, payload: finalResult })
                    } else {
                        return dispatch({ type: GET_PROVIDER_SUCCESS, payload: res })
                    }
                } else {
                    return dispatch({ type: GET_PROVIDER_SUCCESS, payload: res })
                }
            }else{
                return dispatch({ type: GET_PROVIDER_SUCCESS, payload: res })
            }
        } catch (err) {
            return dispatch({ type: GET_PROVIDER_ERROR, payload: err })
        }
    }
}