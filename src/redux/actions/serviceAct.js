import { GET_SERVICES_LOADING, GET_SERVICES_SUCCESS, GET_SERVICES_ERROR } from "../actionTypes"
import { getServicesApi } from '../api';

export const serviceAct = _ => {
    return async(dispatch) => {
        dispatch({type: GET_SERVICES_LOADING})
        try{
            const res = await getServicesApi();
            return dispatch({type: GET_SERVICES_SUCCESS, payload: res.data})
        }catch(err){
            return dispatch({type: GET_SERVICES_ERROR, payload: err})
        }
    }
}