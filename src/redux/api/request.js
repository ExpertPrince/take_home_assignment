import axios from 'axios';
import Config from '../../utils/config';

/**
 * Create an Axios Client with defaults
 */
const client = axios.create({
    baseURL: Config.api_url,
    // headers: {
    //     'Content-Type': 'application/json'
    // }
});

/**
 * Request Wrapper with default success/error actions
 */

 const request = options => {
    const onSuccess = res => {
        return res.data;
    };

    const onError = error => {
        //Response was made but server responded with something other than 2xx
        if(!!error.response){
            console.error('Status: ', error.response.status);
            console.error('Headers: ', error.response.headers);
        }else if(!!error.request){
            console.error('Request Error Message: ', error.message)
        }else{
            console.error('Error Message: ', error.message)
        }

        return Promise.reject(error.response || error.message);
    }

    return client(options)
    .then(onSuccess)
    .catch(onError);
 };

 export default request;

