import request from './request';

export const getServicesApi = _ => {
    return request({
        url: '/services',
        method: 'GET'
    })
}

export const getProvidersApi = _ => {
    return request({
        url: '/providers?include=locations%2Cschedules.location&page%5Bnumber%5D=1&page%5Bsize%5D=10',
        method: 'GET'
    })
}