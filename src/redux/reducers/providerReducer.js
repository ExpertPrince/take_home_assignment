import { GET_PROVIDER_LOADING, GET_PROVIDER_SUCCESS, GET_PROVIDER_ERROR, GET_PROVIDER_SUCCESS_FILTER } from "../actionTypes"

const initialState = {
    providerData: '',
    type: '',
    providerDataFilter: ''
}

export const providerReducer = (state = initialState, { type, payload }) => {
    switch(type) {
        case GET_PROVIDER_LOADING :
            return {
                ...state,
                type
            }
        case GET_PROVIDER_SUCCESS :
            return {
                ...state,
                providerData: payload,
                type
            }
        case GET_PROVIDER_ERROR :
            return {
                ...state,
                providerData: payload,
                type
            }
        case GET_PROVIDER_SUCCESS_FILTER:
            return {
                ...state,
                providerDataFilter: payload,
                type
            }
        default:
            return state;
    }
}