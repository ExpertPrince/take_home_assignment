import {servicesReducer} from './servicesReducer';
import {providerReducer} from './providerReducer';
import { combineReducers } from 'redux';

const reducers = combineReducers({
    services: servicesReducer,
    provider: providerReducer
});

export default reducers;