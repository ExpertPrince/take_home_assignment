import { GET_SERVICES_LOADING, GET_SERVICES_SUCCESS, GET_SERVICES_ERROR } from "../actionTypes"

const initialState = {
    data: '',
    type: '',
}

export const servicesReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case GET_SERVICES_LOADING:
            return {
                ...state,
                data: payload,
                type
            }
        case GET_SERVICES_SUCCESS:
            return {
                ...state,
                data: payload,
                type
            }
        case GET_SERVICES_ERROR:
            return {
                ...state,
                data: payload,
                type
            }
        default:
            return state;
    }
}