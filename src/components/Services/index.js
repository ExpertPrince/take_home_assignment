import React, { useEffect, useState, Fragment } from "react";
import { connect } from "react-redux";
import { serviceAct } from "../../redux/actions/serviceAct";
import { Spinner } from "reactstrap";
import { GET_SERVICES_LOADING } from "../../redux/actionTypes";
import { providerAct } from "../../redux/actions/providerAct";

import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});

const Services = props => {
  const classes = useStyles();
  const [serviceId, setServiceId] = useState(0);
  const [selected, setSelected] = useState();
  useEffect(() => {
    props.serviceAct();
  }, [serviceId]);
  console.log("props", props);

  const _setSelected = item => {
    setSelected(item.id);
    props.providerAct(item.attributes.name);
  };
  return (
    <Fragment>
      {!!props.services && props.services.type === GET_SERVICES_LOADING ? (
        <Spinner />
      ) : (
        <>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="Service Table">
              <TableHead>
                <TableRow>
                  <TableCell>
                    Name{" "}
                    <Button
                      className="pull-right"
                      variant="contained"
                      onClick={() => {props.providerAct(); setSelected('')}}
                    >
                      Clear Filter
                    </Button>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {!!props.services && !!props.services.data ? (
                  props.services.data.length > 0 ? (
                    !!props.services.data &&
                    props.services.data.length > 0 &&
                    props.services.data.map((item, i) => {
                      return (
                        <TableRow
                          key={i}
                          className={selected === item.id ? "active" : ""}
                          id={`toggler-${item.id}`}
                          onClick={() => _setSelected(item)}
                        >
                          <TableCell>{item.attributes.name}</TableCell>
                        </TableRow>
                      );
                    })
                  ) : (
                    <TableRow>
                      <TableCell>
                        <span>No Data found</span>
                      </TableCell>
                    </TableRow>
                  )
                ) : (
                  <TableRow>
                    <TableCell>
                      <span>No Data found</span>
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </Fragment>
  );
};

const mapStateToProps = state => {
  return {
    services: state.services
  };
};

export default connect(mapStateToProps, { serviceAct, providerAct })(Services);
