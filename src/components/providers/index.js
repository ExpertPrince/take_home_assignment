import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Table, Spinner } from "reactstrap";
import {
  GET_PROVIDER_LOADING,
  GET_PROVIDER_SUCCESS_FILTER,
  GET_PROVIDER_SUCCESS
} from "../../redux/actionTypes";
import { providerAct } from "../../redux/actions/providerAct";

import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper
  }
}));

const Providers = props => {
  const classes = useStyles();
  const [providerId, setproviderId] = useState(0);
  const [selected, setSelected] = useState();
  useEffect(() => {
    props.providerAct();
  }, [providerId]);
  const providerData =
    props.providers.type === GET_PROVIDER_SUCCESS_FILTER
      ? props.providers.providerDataFilter
      : GET_PROVIDER_SUCCESS
      ? props.providers.providerData.data
      : [];
  return !!props.providers && props.providers.type === GET_PROVIDER_LOADING ? (
    <Spinner />
  ) : (
    <>
      <List className={classes.root}>
        {!!providerData ? (
          providerData.length > 0 ? (
            providerData.map((item, i) => {
              return (
                <ListItem
                  key={i}
                  className={selected === item.id ? "active" : ""}
                >
                  <ListItemAvatar>
                    <Avatar
                      alt={item.attributes.name}
                      src={
                        item.attributes[`profile-image`] ||
                        "https://vectorified.com/images/avatar-icon-png-9.jpg"
                      }
                    />
                  </ListItemAvatar>
                  <ListItemText
                    primary={item.attributes.name}
                    secondary={
                      <React.Fragment>
                        <Typography
                          component="span"
                          variant="body2"
                          className={classes.inline}
                          color="textPrimary"
                        >
                          {item.attributes.subspecialties.length > 0
                            ? item.attributes.subspecialties[0]
                            : ""}
                        </Typography>
                      </React.Fragment>
                    }
                  />
                </ListItem>
              );
            })
          ) : (
            <ListItem>
              <ListItemText primary="No Data found"></ListItemText>
            </ListItem>
          )
        ) : (
          <ListItem>
            <ListItemText primary="No Data found"></ListItemText>
          </ListItem>
        )}
      </List>
    </>
  );
};

const mapStateToProps = state => {
  return {
    providers: state.provider
  };
};

export default connect(mapStateToProps, { providerAct })(Providers);
